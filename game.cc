#include <Minesweeper.h>
#include <fillRandom.h>

int game::getCount(int x, int y)
{
    int count = 0;
    for(int dx = x-1; dx <= x+1; dx ++)
    {
        for(int dy = y-1; dy <= y+1; dy ++){
            if(dx >= this->width || dy >= this->height || (dx == x && dy == y) || dx < 0 || dy < 0)
            {
                continue;
            }
            if(this->map[this->xyToIndex(dx, dy)] & MAP_HASBOOM)
            {
                count ++;
            }
        }
    }
    return count;
}
int game::getCount(int index)
{
    int x = index % this->height;
    int y = index / this->width;
    return this->getCount(x, y);
}
int game::xyToIndex(int x, int y)
{
    return this->width * y + x;
}
game::game(int width, int height)
{
    mapBlock* map = new mapBlock[height*width]();
    this->constr(width, height, map);
    this->needDelete = true;
}
game::game(int width, int height, mapBlock* map)
{
    this->constr(width, height, map);
}
void game::constr(int width, int height, mapBlock* map)
{
    this->map = map;
    this->openCount = 0;
    this->num = 0;
    int sz = width*height;
    for(int i = 0; i < sz; i ++)
    {
        if(map[i] & MAP_CLICKED)
        {
            this->openCount ++;
        }
        else if(map[i] & MAP_HASBOOM)
        {
            this->num ++;
        }
    }
    this->width = width;
    this->height = height;
    this->needDelete = false;
    this->endat = -1;
}
game::~game()
{
    if(this->needDelete)
    {
        delete this->map;
    }
}
int game::getWidth()
{
    return this->width;
}
int game::getHeight()
{
    return this->height;
}
void game::generateNew(int boomcount)
{
    int boomadded = 0;
    int cap = (this->width * this->height);
    if(boomcount > cap)
    {
        boomcount = cap;
    }
    while(boomadded < boomcount)
    {
        unsigned int xy[2];
        fillRandom(&xy, sizeof(unsigned int)*2);
        int x = xy[0] % this->width;
        int y = xy[1] % this->height;
        int idx = this->xyToIndex(x, y);
        if(this->map[idx] & MAP_HASBOOM)
        {
            continue;
        }
        this->map[idx] |= MAP_HASBOOM;
        boomadded ++;
    }
    this->num = boomadded;
}
int game::click(int x, int y)
{
    int ind = this->xyToIndex(x, y);
    if(!(this->map[ind] & MAP_CLICKED))
    {
        this->openCount ++;
    }
    this->map[ind] |= MAP_CLICKED;
    if(this->map[ind] & MAP_HASBOOM)
    {
        this->endat = ind;
        throw this->endat;
    }
    int rtc = this->getCount(x, y);
    if(rtc == 0)
    {
        for(int dx = x-1; dx <= x+1; dx ++)
        {
            for(int dy = y-1; dy <= y+1; dy ++)
            {
                int di = this->xyToIndex(dx, dy);
                if(!(dx >= this->width || dy >= this->height || (dx == x && dy == y) || dx < 0 || dy < 0) &&
                        (!((this->map[di] & MAP_CLICKED) || (this->map[di] & MAP_HASBOOM))))
                    this->click(dx, dy);
            }
        }
    }
    return rtc;
}
void game::setMark(int x, int y, bool mark)
{
    if(mark)
    {
        this->map[this->xyToIndex(x, y)] |= MAP_MARKED;
    }
    else
    {
        this->map[this->xyToIndex(x, y)] &= ( ~ MAP_MARKED );
    }
}
bool game::isBlockMarked(int x, int y)
{
    return (this->map[this->xyToIndex(x, y)] & MAP_MARKED) > 0;
}
int game::checkBlockCount(int x, int y)
{
    int ind = this->xyToIndex(x, y);
    if((this->map[ind] & MAP_CLICKED) || this->endat >= 0)
    {
        if(this->map[ind] & MAP_HASBOOM)
        {
            return -2;
        }
        return this->getCount(x, y);
    }
    else
    {
        return -1;
    }
}
int game::gendat()
{
    return this->endat;
}
bool game::checkWin()
{
    return (((this->width*this->height)-this->openCount) == this->num && this->endat < 0);
}
