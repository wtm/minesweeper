#include <saver.h>
#include <Minesweeper.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <errno.h>

using namespace std;

char savefile_typespec[] = {'S', 'W', 'E', 'E', 'P', 'E', 'R', 'S', 'A', 'V', 'E'};

gameSave::gameSave(int fd)
{
    this->fd = fd;
    this->g = NULL;
}
gameSave::gameSave(int fd, game* g)
{
    this->fd = fd;
    this->g = g;
}
void gameSave::load()
{
    if(this->g == NULL)
    {
        throw gameLoadingOrSavingError(this, string("Game is NULL."));
    }
    savefileHead sf;
    char* ed = (char*)&sf + sizeof(savefileHead);
    for(char* st = (char*)&sf; st < ed;)
    {
        ssize_t rt = read(this->fd, st, ed - st);
        if(rt == -1)
        {
            throw gameLoadingOrSavingError(this, string("Can't read: ") + string(strerror(errno)));
        }
        st += rt;
    }
    if(memcmp(&(sf.typespec), &savefile_typespec, sizeof(savefile_typespec)) != 0)
    {
        throw gameLoadingOrSavingError(this, string("This is not a save file."));
    }
    size_t szmap = sf.width*sf.height*sizeof(mapBlock);
    char* mapbuf = new char[szmap];
    char* ee = mapbuf + szmap;
    for(char* su = mapbuf; su < ee;)
    {
        ssize_t ru = read(this->fd, su, ee - su);
        if(ru == -1)
        {
            delete mapbuf;
            throw gameLoadingOrSavingError(this, string("Can't read: ") + string(strerror(errno)));
        }
        su += ru;
    }
    this->g->~game();
    this->g->constr(sf.width, sf.height, (mapBlock*)mapbuf);
    this->g->needDelete = 1;
}
void gameSave::save()
{
    if(this->g == NULL)
    {
        throw gameLoadingOrSavingError(this, string("Game is NULL."));
    }
    size_t szmap = this->g->width*this->g->height*sizeof(mapBlock);
    savefileHead h;
    size_t bffsize = szmap + sizeof(savefileHead);
    char* buff = new char[bffsize];
    memcpy(&(h.typespec), &savefile_typespec, sizeof(savefile_typespec));
    h.width = this->g->width;
    h.height = this->g->height;
    memcpy(buff, &h, sizeof(savefileHead));
    memcpy(buff + sizeof(savefileHead), this->g->map, szmap);
    errno = 0;
    ssize_t sst = write(this->fd, buff, bffsize);
    delete buff;
    if(sst < bffsize)
    {
        throw gameLoadingOrSavingError(this, string("Can't write: ") + string(strerror(errno)));
    }
    errno = 0;
    if(lseek(this->fd, 0, SEEK_SET) == -1)
    {
        throw gameLoadingOrSavingError(this, string("Save wroted, but unable to go back to the begining of the file: ") + string(strerror(errno)));
    }
}
gameLoadingOrSavingError::gameLoadingOrSavingError(gameSave* obj, string err)
{
    this->obj = obj;
    this->err = err;
}
string gameLoadingOrSavingError::getError()
{
    return this->err;
}
gameSave* gameLoadingOrSavingError::getObj()
{
    return this->obj;
}
