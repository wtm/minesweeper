CC = g++
DEBUG = --debug
CFLAGS = -std=c++11 -I. -c $(DEBUG)
LFLAGS = $(DEBUG)
OBJS = Minesweeper.o game.o fillRandom.o printer.o autobot.o saver.o
everything: minesw
rebuild: clean everything
clean:
	rm -rf `cat .gitignore`
Minesweeper.o: Minesweeper.cc
	$(CC) $(CFLAGS) Minesweeper.cc -o Minesweeper.o
game.o: game.cc
	$(CC) $(CFLAGS) game.cc -o game.o
fillRandom.o: fillRandom.cc
	$(CC) $(CFLAGS) -Wno-pointer-arith fillRandom.cc -o fillRandom.o
printer.o: printer.cc
	$(CC) $(CFLAGS) printer.cc -o printer.o
autobot.o: autobot.cc
	$(CC) $(CFLAGS) autobot.cc -o autobot.o
saver.o: saver.cc
	$(CC) $(CFLAGS) saver.cc -o saver.o
minesw: $(OBJS) Makefile
	$(CC) $(OBJS) $(LFLAGS) -o minesw
