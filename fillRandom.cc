#include <Minesweeper.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <fillRandom.h>

int randfd = -1;
void fillRandom(void* buffer, size_t sz)
{
    if(randfd == -1)
    {
        randfd = open("/dev/urandom", O_RDONLY);
        if(randfd == -1)
        {
            throw errno;
        }
    }
    ssize_t readCount = 0;
    void* readTo = buffer;
    while(readCount < sz)
    {
        ssize_t rc = read(randfd, readTo, sz - readCount);
        if(rc >= 0)
        {
            readCount += rc;
            readTo = buffer + readCount;
        }
        else if (rc == -1)
        {
            throw errno;
        }
    }
}

