#include <printer.h>
#include <string>
#include <Minesweeper.h>
#include <iomanip>
#include <iostream>

using namespace std;

printer::printer(game* g)
{
    this->g = g;
}
void printer::print(ostream& s, bool useColor)
{
    string gametit;
    gametit.reserve(10);
    gametit+=to_string(this->g->getWidth());
    gametit+="x";
    gametit+=to_string(this->g->getHeight());
    if(gametit.size() < (10-5))
        gametit.insert(0, "Game ");
    s << setw(10) << gametit << endl;
    s << " " << string(this->g->getWidth()*2+8, '=').c_str() << endl;
    for(int line = 0; line < this->g->getHeight(); line ++)
    {
        s << setw(3) << line << " | ";
        for(int cow = 0; cow < this->g->getWidth(); cow ++)
        {
            if(useColor) s << "\033[37m";
            s << setw(2) << cow << " ";
            int b = this->g->checkBlockCount(cow, line);
            int cc = ((b % 6) + 1);
            if(cc < 0)
                cc = 1;
            if(useColor) s << "\033[3" << cc << "m";
            if(this->g->isBlockMarked(cow, line))
            {
                s << "M";
            }
            else if(b > 0)
            {
                s << to_string(b);
            }
            else if(b == 0)
            {
                s << ".";
            }
            else if(b == -1)
            {
                s << "?";
            }
            else
            {
                s << "!";
            }
            if(useColor) s << "\033[39m";
            s << " ";
        }
        s << " |" << endl;
    }
}

