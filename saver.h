#ifndef saver_H
#define saver_H

#include <string>
#include <stdint.h>
#include <Minesweeper.h>

using namespace std;

class gameSave
{
    protected:
        int fd;
        game* g;
    public:
        gameSave(int fd);
        gameSave(int fd, game* g);
        void load();
        void save();
        void save(game* g);
        game* getGame();
        void setGame(game* g);
};
class gameLoadingOrSavingError
{
    private:
        gameSave* obj;
        string err;
    public:
        gameLoadingOrSavingError(gameSave* obj, string err);
        string getError();
        gameSave* getObj();
};
struct savefileHead
{
    char typespec[11];
    int32_t width;
    int32_t height;
};
extern char savefile_typespec[11];
#endif
