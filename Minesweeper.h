#ifndef Minesweeper_H
#define Minesweeper_H

#include <stdint.h>
#include <stdlib.h>

struct prasedArgs {
    int mode = 0;
    size_t width = 10;
    size_t height = 8;
    unsigned int num = 10;
};
#define mapBlock uint8_t
#define MAP_HASBOOM 1
#define MAP_MARKED 2
#define MAP_CLICKED 4
class game {
    friend class gameSave;
    private:
        mapBlock* map;
        int width;
        int height;
        int num;
        bool needDelete;
        int getCount(int x, int y);
        int getCount(int index);
        int endat;
        void constr(int width, int height, mapBlock* map);
        int openCount;
    protected:
        int xyToIndex(int x, int y);
    public:
        game(int width, int height);
        game(int width, int height, mapBlock* map);
        ~game();
        int getWidth();
        int getHeight();
        void generateNew(int boomcount);
        int click(int x, int y);
        void setMark(int x, int y, bool mark);
        bool isBlockMarked(int x, int y);
        int checkBlockCount(int x, int y);
        int gendat();
        bool checkWin();
};
#endif
