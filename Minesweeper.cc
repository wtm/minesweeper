#include <iostream>
#include <unistd.h>
#include <Minesweeper.h>
#include <fillRandom.h>
#include <printer.h>
#include <autobot.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <saver.h>

using namespace std;

void printHelp ()
{
    cerr << program_invocation_name << ": Usage: "
        << program_invocation_short_name
        << " < -g|-b : Start game (-b: with bot) [ -w width -h height -n number of bomb ] > | < -l : Load from ./minesweeper.save >"
        << endl;
}
prasedArgs praseArguments ( int argc, char **argv )
{
    int opt;
    prasedArgs args;
    while ( ( opt = getopt ( argc, argv, "glbw:h:n:" ) ) != -1 )
    {
        switch ( opt )
        {
            case 'g':
                args.mode = 1;
                break;
            case 'b':
                args.mode = 2;
                break;
            case 'l':
                args.mode = 3;
                break;
            case 'w':
                args.width = atoi ( optarg );
                break;
            case 'h':
                args.height = atoi ( optarg );
                break;
            case 'n':
                args.num = atoi ( optarg );
                break;
            default:
                printHelp();
                exit(1);
        }
    }
    if ( ! args.mode )
    {
        cerr << program_invocation_name << ": Mode not specified." << endl;
        printHelp();
        exit(2);
    }
    return args;
}
int main ( int argc, char **argv )
{
    prasedArgs pa = praseArguments ( argc, argv );
    const char* savefilename = "./minesweeper.save";
    int fd;
    gameSave* gs;
    if(pa.mode == 1 || pa.mode == 2 || pa.mode == 3)
    {
        game g(pa.width, pa.height);
        if(pa.mode == 3)
        {
            fd = open(savefilename, O_RDONLY);
            if(fd == -1)
            {
                int ei = errno;
                cout << "Unable to load: " << strerror(ei) << endl;
                return ei;
            }
            gs = new gameSave(fd, &g);
            gs->load();
            delete gs;
            close(fd);
        }
        else
        {
            g.generateNew(pa.num);
        }
        printer pt(&g);
        autobot bot(&g);
        bool usecolor = isatty(1);
        while(g.gendat() < 0 && !(g.checkWin()))
        {
            pt.print(cout, usecolor);
            cout << " <c|m|b> <x> <y> ] ";
            char c;
            int x, y;
            cin >> c;
            cin >> x;
            cin >> y;
            int actionTotal;
            switch(c)
            {
                case 'c':
                    try
                    {
                        g.click(x, y);
                    }
                    catch (int endat)
                    {
                        cout << "Sorry, boomed." << endl;
                        pt.print(cout, usecolor);
                        return 1;
                    }
                    break;
                case 'm':
                    g.setMark(x, y, !g.isBlockMarked(x, y));
                    break;
                case 'b':
                    try
                    {
                        bot.solve(x, y);
                    }
                    catch (botSuckedError& e)
                    {
                        cout << "You must given a known block to use bot." << endl;
                    }
                    break;
                case 'g':
                    try {
                        bot.guess(x, y);
                    } catch (botSuckedError& e) {
                        if(g.gendat() >= 0) {
                            cout << "Sorry. Faild at " << x << ", " << y << endl;
                            pt.print(cout, usecolor);
                            exit(1);
                        }
                    }
                case 'f':
                    for(int x = 0; x < g.getWidth(); x ++)
                    {
                        for(int y = 0; y < g.getHeight(); y ++)
                        {
                            try
                            {
                                bot.solve(x, y);
                            }
                            catch (botSuckedError& e)
                            {}
                        }
                    }
                    break;
                case 'F':
                    actionTotal = 0;
                    for(int x = 0; x < g.getWidth(); x ++)
                    {
                        for(int y = 0; y < g.getHeight(); y ++)
                        {
                            try
                            {
                                actionTotal += bot.solve(x, y);
                            }
                            catch (botSuckedError& e)
                            {}
                        }
                    }
                    if(actionTotal == 0) {
                        for(int x = 0; x < g.getWidth(); x ++)
                        {
                            for(int y = 0; y < g.getHeight(); y ++)
                            {
                                try
                                {
                                    bot.guess(x, y);
                                }
                                catch (botSuckedError& e)
                                {
                                    if(g.gendat() >= 0) {
                                        cout << "Guess faild, sorry!" << endl;
                                        pt.print(cout, usecolor);
                                        exit(1);
                                    }
                                }
                            }
                        }
                    }
                case 's':
                    fd = open(savefilename, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
                    if(fd == -1)
                    {
                        cout << "Error opening file: " << strerror(errno) << endl;
                        break;
                    }
                    if(ftruncate(fd, 0) == -1)
                    {
                        cout << "Error truncating file: " << strerror(errno) << endl;
                        break;
                    }
                    gs = new gameSave(fd, &g);
                    try
                    {
                        gs->save();
                    }
                    catch (gameLoadingOrSavingError& e)
                    {
                        cout << e.getError() << endl;
                    }
                    delete gs;
                    close(fd);
                    fd = -1;
                    break;
                default:
                    cout << "command not find. availables are c, m, b." << endl;
            }
            if(pa.mode == 2)
            {
                for(int x = 0; x < g.getWidth(); x ++)
                {
                    for(int y = 0; y < g.getHeight(); y ++)
                    {
                        try
                        {
                            bot.solve(x, y);
                        }
                        catch (botSuckedError& e)
                        {}
                    }
                }
            }
        }
        if(g.checkWin())
        {
            pt.print(cout, usecolor);
            if(usecolor) cout << "\033[35m";
            cout << "You win!" << endl;
        }
    }
    return 0;
}
