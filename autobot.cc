#include <autobot.h>
#include <iostream>
#include <assert.h>

using namespace std;

autobot::autobot(game* g)
{
    this->g = g;
}
int autobot::solve(int x, int y)
{
    int ct = this->g->checkBlockCount(x, y);
    if(ct < 0)
    {
        throw botSuckedError(BOTSUCK_GIVENBLOCKUNKNOW);
    }
    int unknow = 0;
    int notOpen = 0;
    int marked = 0;
    int actionDone = 0;
    for(int dx = x-1; dx <= x+1; dx ++)
    {
        for(int dy = y-1; dy <= y+1; dy ++)
        {
            if(dx < 0 || dy < 0 || dx >= this->g->getWidth() || dy >= this->g->getHeight() || (dx == x && dy == y))
            {
                continue;
            }
            if((!this->g->isBlockMarked(dx, dy)) && (this->g->checkBlockCount(dx, dy) < 0))
            {
                unknow ++;
            }
            if((this->g->checkBlockCount(dx, dy) < 0))
            {
                notOpen ++;
            }
            if(this->g->isBlockMarked(dx, dy))
            {
                marked ++;
            }
        }
    }
    for(int dx = x-1; dx <= x+1; dx ++)
    {
        for(int dy = y-1; dy <= y+1; dy ++)
        {
            if(dx < 0 || dy < 0 || dx >= this->g->getWidth() || dy >= this->g->getHeight() || (dx == x && dy == y))
            {
                continue;
            }
            if((!this->g->isBlockMarked(dx, dy)) && (this->g->checkBlockCount(dx, dy) < 0))
            {
                if(marked == ct)
                {
                    try
                    {
                        this->g->click(dx, dy);
                        actionDone++;
                    }
                    catch(int e)
                    {
                        cerr << "Bot are not expected to get error, This is a error. ( Core dumped )" << endl;
                        assert(0);
                        throw e;
                    }
                }
                else if(notOpen == ct)
                {
                    this->g->setMark(dx, dy, true);
                    actionDone++;
                }
            }
        }
    }
    return actionDone;
}
void autobot::guess(int x, int y)
{
    this->solve(x, y);
    for(int dx = x - 1; dx <= x + 1; dx ++) {
        for(int dy = y - 1; dy <= y + 1; dy ++) {
            if(dx < 0 || dy < 0 || dx >= this->g->getWidth() || dy >= this->g->getHeight() || (dx == x && dy == y))
            {
                continue;
            }
            if((!this->g->isBlockMarked(dx, dy)) && (this->g->checkBlockCount(dx, dy) < 0)) {
                try { this->g->click(dx, dy); } catch (int e) {
                    throw botSuckedError(BOTSUCK_GUESSFAILD);
                }
                this->solve(dx, dy);
                return;
            }
        }
    }
}

botSuckedError::botSuckedError(int reason)
{
    this->reason = reason;
}
int botSuckedError::getReason()
{
    return this->reason;
}
