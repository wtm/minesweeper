#ifndef printer_H
#define printer_H

#include <iostream>
#include <Minesweeper.h>
using namespace std;

class printer
{
    protected:
        game* g;
    public:
        printer(game* g);
        void print(ostream& s, bool useColor);
};
#endif
