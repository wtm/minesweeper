#ifndef autobot_H
#define autobot_H

#include <Minesweeper.h>
class autobot
{
    protected:
        game* g;
    public:
        autobot(game* g);
        int solve(int x, int y);
        void guess(int x, int y);
};
#define BOTSUCK_GIVENBLOCKUNKNOW 1
#define BOTSUCK_GUESSFAILD 2
class botSuckedError
{
    private:
        int reason;
    public:
        botSuckedError(int reason);
        int getReason();
};
#endif
